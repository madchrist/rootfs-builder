rootfs-builder
==============

Create a root filesystem image from a step by step recipe


Features
--------

* can generate a root filesystem image in the form of a tar archive, an initram or a squashfs archive
* support chrooting into a tar archive, an initram or a squashfs archive, for inspection purposes
* support for xattrs
* automatic CPU emulation when needed


Requirements
------------

* Linux
* Perl 5+
* IPC::Run
* TMPFS support

Other OSes might work but are not supported.


CPU Emulation
-------------

If the image you build uses another CPU architecture than the host, you will need:

* qemu, build statically, with support for the target CPU arch
* a kernel with binfmt support (CONFIG\_BINFMT\_MISC)
* the binfmt FS mounted
* the binfmt interpreter for the wanted arch activated

All this is pretty standard and can be as simple as installing qemu and starting the binfmt service.


How does it work
----------------

rootfs-builder looks in the current directory for the steps of the recipe.  
Each step is a directory containing files and/or scripts to modify the image. Steps are applied successively, in alphabetical order.  
A tar archive of the image is generated at the end of each step to save progress and to serve as a base for the next step.  
A step archive, usually the last one, can then be converted into an initram or a squashfs archive.  

If a step archive already exists, the step is only executed if either:
* A file in the step directory is newer than the step archive
* The previous step was executed

This is similar to *make*'s logic and ensures steps are only executed when something has changed.


### Steps

A step is a directory named *NNname*.  
*NN* is the step's ID, *name* is just informative.  
Examples: *00bootstrap*, *42foo*, *75bar*, *97057*

Each step generates a step archive named *NN.tar*.  
Examples: *00.tar*, *42.tar*, *75.tar*, *97.tar*


#### Substeps
Multiple steps can share the same step ID, they are then considered substeps of a single step and will behave as a group (if one needs to be executed, all are).  
Example: *42foo*, *421bar*, *422_barfoo*, *422_foobar*  
These are all part of step 42 and only one step archive will be generated: *42.tar*.  
Substeps are executed in alphabetical order, just like regular steps.


#### Content
A step directory can contain up to 3 things:

* *chroot_copy*: a directory whose content is copied directly into the image
* *host.run*: a script to run directly on the host
* *chroot.run*: a script to run while chrooted in the image

All these are optional, but at least one of them is needed for the step to do anything.  
They are applied in this order.


##### chroot_copy

The content of this directory is copied directly to the root of the image.  
`cp -a --no-preserve=owner` is used internaly, i.e.: everything is preserved except owner and group. Everything will belong to root:root.  
Owner and group cannot be preserved because a user/group from the host could have a different ID or not exist at all in the image. If you need to set user/group, do it from the *chroot.run* script.


##### host.run

This script is run directly on the host, as root.  
The script will be copied to the root of the image and will be executed there. This means that, for the script, *./* is the root of the image.  
**WARNING**: Since the script is running directly on the host: */* is STILL the root of the host ! Make sure your paths start with *./* and not */* when you target the image.  
This script should only be used in the first step to bootstrap the image.  


##### chroot.run

This script is run while chrooted in the image, as root.  
For *chroot.run*, the current directory is */*, the root of the image.  
/dev, /proc and /sys will automatically be mounted in the chroot (with --rbind) if they exist.



Build mode
----------

Build all steps, or up to <step> if given

Simply run:

    # rootfs-builder build

You will be presented with the list of steps to be built and asked for confirmation before starting.  
The result of each step is stored into the archive *NN.tar*.  

You can also give a specific step to stop to:

    # rootfs-builder build 42
    
    

Initram mode
------------

Create an initram from a given archive

Example:

    # rootfs-builder initram 42.tar my_new_initram

You can compress the initram by adding --xz, --bzip2 or --gzip. LZO and LZ4 compressions are not supported for the moment

    # rootfs-builder initram 42.tar my_new_xz_initram --xz

You can use another initram as the source or even a squashfs archive

    # rootfs-builder initram my_gzipped_initram my_xzed_initram --xz
    # rootfs-builder initram my_squashfs my_xzed_initram --xz



Squashfs mode
-------------

Create a squashfs archive from a given archive

Example:

    # rootfs-builder squashfs 42.tar my_squashfs

You can compress the squashfs with --xz

    # rootfs-builder squashfs 42.tar my_xz_squashfs --xz

You can use another squashfs archive as the source or even an initram

    # rootfs-builder squashfs my_squashfs my_xz_squashfs --xz
    # rootfs-builder squashfs my_initram my_squashfs
    


Shell mode
----------

Load a given archive, chroot into it and open a shell.  
This is for debugging purpose only, it does NOT save the modifications to the archive.

Examples:

    # rootfs-builder shell 42.tar
    # rootfs-builder shell my_initram
    # rootfs-builder shell my_rootfs.squash

This will extract the image into a temporary directory, chroot into it and execute /bin/sh, when finished simply exit from the shell and everything will be cleaned up.  
/dev, /proc and /sys will be available in the chroot, like they are for the *chroot.run* script.  

For initrams, the compression is automatically detected but only xz, bzip2 and gzip are supported.  
For squashfs, anything supported by unsquashfs is supported.
