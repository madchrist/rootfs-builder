#!/usr/bin/env perl
use strict;
use warnings;
use experimental 'smartmatch';
use English;
use Getopt::Long;
use File::Find;
use Cwd qw/abs_path cwd/;
use IPC::Run qw/run/;

my $cwd = cwd();
my $work_dir = $cwd . '/.rootfs-builder';
my $work_dir_size = '16G'; # warning: the work dir is a tmpfs mount, make sure you have enough memory
my $work_archive = $cwd . '/.rootfs-builder.tmp'; # temporary file used when opening/creating archives

sub help
{
    print('Usage: rootfs-builder build [<step-number>] [-d|--drop-to-shell] [-y|--yes] [-s|--show-new]' . "\n" .
          '  or   rootfs-builder initram <archive> <initram> [--xz|--bzip2|--gzip]' . "\n" .
          '  or   rootfs-builder squashfs <archive> <squashfs-file> [--xz]' . "\n" .
          '  or   rootfs-builder shell <archive>' .  "\n" .
          '  or   rootfs-builder clean' .  "\n" .
          '  or   rootfs-builder [help|-h|--help]' . "\n" .
          "\n" .
          'build:' . "\n" .
          '  Build all steps, or up to <step-number> if given' . "\n" .
          '    -d|--drop-to-shell    If the chroot.run script returns non-zero, start a shell in the chroot. The step will NOT be saved' . "\n" .
          '    -y|--yes              Start building without asking for confirmation' . "\n" .
          '    -s|--show-new         For each step, list the files newer than the step\'s archive' . "\n" .
          "\n" .
          'initram:' . "\n" .
          '  Create an initram from a given archive' . "\n" .
          '  If multiple compressions are given, the last one is used' . "\n" .
          '    --bzip2               Compress with bzip2' . "\n" .
          '    --gzip                Compress with gzip' . "\n" .
          '    --xz                  Compress with xz (using the parameters required by the linux kernel)' . "\n" .
          "\n" .
          'squashfs:' . "\n" .
          '  Create a squashfs archive from a given archive' . "\n" .
          '    --xz                  Compress with xz' . "\n" .
          "\n" .
          'shell:' . "\n" .
          '  Load a given archive, chroot into it and open a shell' . "\n" .
          '  This is for debugging purposes only, the modifications are NOT saved' . "\n" .
          "\n" .
          'clean:' . "\n" .
          '  Clean any remain from a previous run, only usefull if this program bugged' . "\n" .
          '  This is done automatically at the start of all modes' . "\n" .
          "\n");
}






# parse cli arguments
my %options = ('compression' => 'none');
GetOptions(\%options, 'xz', 'bzip2', 'gzip', 'help|h', 'drop-to-shell|d', 'yes|y', 'show-new|s') or exit(1);
foreach('xz', 'bzip2', 'gzip') { if(defined($options{$_})) { $options{'compression'} = $_; delete($options{$_}); } }

# help mode
if(@ARGV == 0 || exists($options{'help'}) || $ARGV[0] eq 'help') { help(); exit(0); }

# common to any other modes
my $mode = shift(@ARGV);
must_be_root();
clean();


# mode dispatch
if($mode eq 'build')
{
    args_needed(0, 1);
    my $final_step = 100;
    if(@ARGV == 1)
    {
        if($ARGV[0] !~ /^\d\d$/) { fatal('<step-number> must be a two digit number'); }
        $final_step = $ARGV[0];
    }
    build($final_step);
}
elsif($mode eq 'initram')
{
    args_needed(2);
    my ($archive, $initram) = ($ARGV[0], abs_path($ARGV[1]));

    rootfs_setup();
    rootfs_load($archive);

    # initrams need a /init
    if(! -e rootfs_path('/init')) { system_cyan('ln', '-s', 'sbin/init', rootfs_path('/init')); }

    rootfs_save($initram, 'cpio', $options{'compression'});
}
elsif($mode eq 'squashfs')
{
    args_needed(2);
    my ($archive, $squashfs) = ($ARGV[0], abs_path($ARGV[1]));

    rootfs_setup();
    rootfs_load($archive);
    rootfs_save($squashfs, 'squashfs', $options{'compression'});
}
elsif($mode eq 'shell')
{
    args_needed(1);
    my ($archive) = ($ARGV[0]);
    delete($options{'drop-to-shell'}); # this option has no meaning here and would start another shell if the first returned non-zero

    rootfs_setup();
    rootfs_load($archive);
    chroot_run('/bin/sh');
}
elsif($mode eq 'clean')
{
    args_needed(0);
}
else
{
    fatal('unknown mode');
}

# common to all modes
clean();
exit(0);





sub clean
{
    rootfs_clean();
    if(-e $work_archive) { system_cyan('rm', '--', $work_archive); }
}


sub build
{
    my ($last_step_index) = @_;

    # find steps and substeps
    my (%steps, $i);
    foreach(sort(glob('[0-9][0-9]*')))
    {
        if(! -d) { next; }  # only directories can be steps
        $i = substr($_, 0, 2);  # step id
        if($i > $last_step_index) { last; }  # skip unrequested steps
        if(! exists($steps{$i})) { $steps{$i} = { 'substeps' => [] }; }
        push(@{$steps{$i}{'substeps'}}, $_);
    }
    if(keys(%steps) == 0) { fatal('No step found !'); }

    # find source and destination archives for each steps, also detect which steps need to be ran or not
    my $previous_i;
    foreach $i (sort(keys(%steps)))
    {
        # source archive
        $steps{$i}{'source'} = undef;
        if(defined($previous_i)) { $steps{$i}{'source'} = $steps{$previous_i}{'destination'}; }

        # destination archive
        $steps{$i}{'destination'} = $i . '.tar';

        # determine if step needs to run or not
        if   (defined($previous_i) && $steps{$previous_i}{'state'} ne 'cached')                                { $steps{$i}{'state'} = 'outdated'; }
        elsif(! -e $steps{$i}{'destination'})                                                                  { $steps{$i}{'state'} = 'new'; }
        elsif(defined($steps{$i}{'source'}) && mtime($steps{$i}{'source'}) > mtime($steps{$i}{'destination'})) { $steps{$i}{'state'} = 'outdated'; }
        elsif(mtime(@{$steps{$i}{'substeps'}}) > mtime($steps{$i}{'destination'}))                             { $steps{$i}{'state'} = 'modified'; }
        else                                                                                                   { $steps{$i}{'state'} = 'cached'; }

        $previous_i = $i;
    }

    # show what will be done
    foreach my $i (sort(keys(%steps)))
    {
        print_cyan_bold('[' . sprintf('%-8s', $steps{$i}{'state'}) . '] ' . $i);
        foreach my $substep (@{$steps{$i}{'substeps'}})
        {
            print_cyan_bold('            \_ ' . $substep);
            if($steps{$i}{'state'} ne 'cached' && $options{'show-new'})
            {
                # show substep files newer than destination
                my $dmtime = mtime($steps{$i}{'destination'});
                find({ 'wanted' => sub { if(-e $_) { my $mtime = (stat($_))[9]; if($mtime > $dmtime) { print_green('                \_ ' . $File::Find::name); } } }, 'follow' => 1, 'follow_skip' => 2 }, ($substep));
            }
        }
    }

    # ask user for confirmation
    if(! $options{'yes'})
    {
        print_cyan_bold('Proceed ? [Y/n] ');
        if(<STDIN> !~ /^(|y|yes)$/i) { exit(0); }
    }

    # run steps
    foreach my $i (sort(keys(%steps)))
    {
        if($steps{$i}{'state'} eq 'cached') { next; }
        build_step($i, $steps{$i}{'source'}, \@{$steps{$i}{'substeps'}}, $steps{$i}{'destination'});
    }
}


sub build_step
{
    my ($i, $source, $substeps, $destination) = @_;

    print_cyan_bold('<STEP ' . $i .'>');
    rootfs_setup();
    if(defined($source)) { rootfs_load($source); }

    foreach my $substep (@{$substeps})
    {
        print_cyan_bold('<SUBSTEP ' . $substep .'>');

        # "chroot_copy" directory
        my $chroot_copy = $substep . '/chroot_copy';
        if(-e $chroot_copy)
        {
            system_cyan('cp', '-aTv', '--no-preserve=owner', $chroot_copy, rootfs_path('/')); # -T so only the content of the directory is copied, not the directory itself
        }

        # "host.run" script
        my $host_run = $substep . '/host.run';
        if(-e $host_run)
        {
            system_cyan('cp', '--preserve=mode', $host_run, rootfs_path('/'));
            chdir(rootfs_path('/'));

            system_cyan('./host.run');

            chdir($cwd);
            system_cyan('rm', '-f', rootfs_path('/host.run'));
        }

        # "chroot.run" script
        my $chroot_run = $substep . '/chroot.run';
        if(-e $chroot_run)
        {
            system_cyan('cp', '--preserve=mode', $chroot_run, rootfs_path('/'));

            chroot_run('./chroot.run');

            system_cyan('rm', '-f', rootfs_path('/chroot.run'));
        }

        print_cyan_bold('</SUBSTEP ' . $substep .'>');
    }

    # pack destination
    print_cyan_bold('Save step archive');
    rootfs_save($destination, 'tar');

    # clean up
    print_cyan_bold('Clean up');
    rootfs_clean();
    print_cyan_bold('</STEP ' . $i .'>');
}





sub chroot_run
{
    my (@cmd) = @_;
    my ($qemu, $pid);

    # mounts
    print_cyan_bold('Mount /proc, /sys and /dev in rootfs directory');
    foreach('/proc', '/sys', '/dev') { if(-e rootfs_path($_)) { system_cyan('mount', $_, '--rbind', rootfs_path($_)); } }

    # cpu emulation, if needed
    $qemu = file_emulator(rootfs_path('/lib/libc.so.6'));
    if(defined($qemu))
    {
        print_cyan_bold('Enabling cpu emulation with: ' . $qemu);
        system_cyan('cp', '-a', $qemu, rootfs_path($qemu));
    }

    # fork => parent wait, child chroot and run script
    print_purple_bold('<chroot>');
    $pid = fork();
    if(! defined($pid)) { fatal('Fork failed'); }
    if($pid == 0)
    {
        chroot(rootfs_path('/')) && chdir('/') or die;
        $ENV{'PS1'} = '(chroot) # '; # debian's shell, dash, does not support colors in PS1, so we use a very minimal prompt
        my $r = system(@cmd);
        if ($r != 0)
        {
            if ($options{'drop-to-shell'}) { print_purple_bold("\n" . 'Chroot command failed, dropping to shell as requested' . "\n"); system('/bin/sh'); }
            exit(1);
        }

        exit(0);
    }

    # wait for child, disable C-c in the parent while waiting so that only the child receives it, otherwise C-c would kill the parent and the child would become unstoppable
    $SIG{INT} = 'IGNORE'; waitpid($pid, 0); $SIG{INT} = 'DEFAULT';
    my $r = $?;
    print_purple_bold('</chroot>');
    if($? != 0) { fatal('Chroot script failed !'); }

    # clean up
    print_cyan_bold('Clean up chroot');
    foreach(rootfs_mounts()) { umount($_); }
    if(defined($qemu)) { system_cyan('rm', '-f', rootfs_path($qemu));  }
}




sub rootfs_path { return($work_dir . '/rootfs/' . shift); }


sub rootfs_setup
{
    system_cyan('mkdir', $work_dir);
    system_cyan('mount', 'none', '-t', 'tmpfs', '-o', 'size=' . $work_dir_size . ',mode=0700', $work_dir);
    system_cyan('mkdir', rootfs_path('/'));
}


sub rootfs_clean
{
    foreach(rootfs_mounts()) { umount($_); }
    if(-e $work_dir)
    {
        umount($work_dir);
        system_cyan('rmdir', $work_dir);
    }
}


sub rootfs_mounts
{
    my (@mounts);
    foreach(file_read('/proc/mounts')) { if(/^\S+\s+(\Q$work_dir\E\/rootfs\/\S+)\s+/) { push(@mounts, $1); } }

    # sort by depth, deepest first, top last
    @mounts = sort {$b cmp $a} @mounts;

    return(@mounts);
}


sub rootfs_load
{
    my ($path) = @_;

    # scan file to detect its format
    my $format = run_cyan([ 'file', '-Lz', '--', $path ]);

    # tar archive
    if($format =~ /tar archive/)
    {
        # tar handles compression automatically
        system_cyan('tar', '--numeric-owner', '--xattrs-include=*.*', '-xf', $path, '-C', rootfs_path('/'));
    }

    # cpio archive
    elsif($format =~ /cpio archive/)
    {
        # load file in memory
        my $content = file_read($path);

        # decompress if needed
        if   ($format =~ /XZ compressed/)    { $content = run_cyan(['unxz'], $content); }
        elsif($format =~ /bzip2 compressed/) { $content = run_cyan(['bunzip2'], $content); }
        elsif($format =~ /gzip compressed/)  { $content = run_cyan(['gunzip'], $content); }

        # extract cpio to rootfs
        run_cyan([ 'cpio', '-i', '-D', rootfs_path('/') ], $content);
    }

    # squashfs archive
    elsif($format =~ /Squashfs filesystem/)
    {
        # squashfs handles compression automatically, -f because the directory already exists
        system_cyan('unsquashfs', '-f', '-d', rootfs_path('/'), $path);
    }

    # unknown archive
    else { fatal('Unknown archive format'); }
}


sub rootfs_save
{
    my ($path, $archive_type, $compression_type) = @_;

    if   ($archive_type eq 'tar')
    {
        # --numeric-owner needed to avoid mixing host and chroot users
        system_cyan('tar', '--auto-compress', '--numeric-owner', '--xattrs-include=*.*', '-cf', $work_archive, '-C', rootfs_path('/'), '.');
    }

    elsif($archive_type eq 'cpio')
    {
        # find paths to include in the archive, identical to "cd rootfs_dir && find"
        my @paths = ();
        find(sub { push(@paths, '.' . substr($File::Find::name, length(abs_path(rootfs_path('/'))))); }, rootfs_path('/'));

        # create cpio archive
        run_cyan([ 'cpio', '--quiet', '-H', 'newc', '-o', '--file', $work_archive, '-D', rootfs_path('/') ], join("\n", @paths) . "\n");

        # compress if needed
        if(defined($compression_type))
        {
            my $tmp = $work_archive;
            if   ($compression_type eq 'xz'   ) { system_cyan('xz', '-v', '-T', '0', '--check=crc32', '-9e', $tmp); $tmp .= '.xz';  } # the kernel only supports a specific type of xz
            elsif($compression_type eq 'bzip2') { system_cyan('bzip2', '-p', '--best', $tmp);                       $tmp .= '.bz2'; }
            elsif($compression_type eq 'gzip' ) { system_cyan('gzip', '--best', $tmp);                              $tmp .= '.gz';  }
            system_cyan('mv', '--', $tmp, $work_archive);
        }
    }

    elsif($archive_type eq 'squashfs')
    {
        my @cmd = ('mksquashfs', rootfs_path('/'), $work_archive);

        if($compression_type eq 'xz') { push(@cmd, '-comp', 'xz', '-b', '1M', '-Xdict-size', '100%'); }
        system_cyan(@cmd);
    }

    else { fatal('invalid parameters'); }

    # move to user requested path
    system_cyan('mv', '--', $work_archive, $path);
}


sub file_emulator
{
    my ($path) = @_;

    my ($content, $i, @binfmt_lines, $match, $binfmt, $magic_hex, $mask_hex);

    # make sure the file even exists
    if(! -e $path) { return(undef); }

    # read the file's content
    $content = file_read($path);

    foreach $binfmt (glob("/proc/sys/fs/binfmt_misc/*"))
    {
        if($binfmt =~ /(register|status)$/) { next; }

        # read binfmt specs
        @binfmt_lines = file_read($binfmt);

        # skip unknown stuff
        if(@binfmt_lines != 6) { next; }
        if(length($binfmt_lines[4]) != 47) { next; }

        # test if the file's magic number matches the binfmt
        $match = 1;
        $magic_hex = substr($binfmt_lines[4], 6, 40);
        $mask_hex = substr($binfmt_lines[5], 5, 40);
        for($i = 0; $i < 20; $i++) { if((ord(substr($content, $i, 1)) & hex(substr($mask_hex, $i*2, 2))) != hex(substr($magic_hex, $i*2, 2))) { $match = 0; } }
        if(!$match) { next; }

        if($binfmt_lines[1] =~ /^interpreter\s+(.+)$/) { return($1); }
    }

    # nothing found, probably no emulation needed
    return(undef);
}










#######################################
#                             _       #
#   __ _  ___ _ __   ___ _ __(_) ___  #
#  / _` |/ _ \ '_ \ / _ \ '__| |/ __| #
# | (_| |  __/ | | |  __/ |  | | (__  #
#  \__, |\___|_| |_|\___|_|  |_|\___| #
#  |___/                              #
#                                     #
#######################################


sub args_needed
{
    my ($min, $max) = @_;
    if(! defined($max)) { $max = $min; }
    if(@ARGV < $min) { fatal('not enough arguments'); }
    if(@ARGV > $max) { fatal('too many arguments'); }
}


sub file_read
{
    my ($path) = @_;
    my ($fh, $content);
    open($fh, '<', $path) or die 'Could not open file ' . $path . ': ' . $!;
    $content = do { local $/; <$fh> };
    close($fh);
    if(wantarray) { return(split(/^/, $content)); }
    else { return($content); }
}


sub mtime
{
    my @paths = @_;
    my $max = 0;
    foreach my $path (@paths)
    {
        find({ 'wanted' => sub { if(-e $_) { my $mtime = (stat($_))[9]; if($mtime > $max) { $max = $mtime; } } }, 'follow' => 1, 'follow_skip' => 2 }, ($path));
    }
    return($max);
}


sub umount
{
    my ($path) = @_;
    if(system('mountpoint', '-q', $path) == 0) { system_cyan('umount', $path); }
}


sub run_cyan
{
    my ($cmd, $stdin) = @_;
    print_cyan(join(' ', @$cmd));
    if(! defined($stdin)) { $stdin = ''; }
    my($stdout, $stderr);
    my $r = run($cmd, \$stdin, \$stdout, \$stderr);
    if(!$r) { die($stderr); }
    return($stdout);
}


# one liners
sub system_cyan       { print_cyan(join(' ', @_)); system(@_) == 0 or die; }
sub print_green       { print("\e[0;32m" . shift() . "\e[0m" . "\n"); }
sub print_cyan        { print("\e[0;36m" . shift() . "\e[0m" . "\n"); }
sub print_cyan_bold   { print("\e[1;36m" . shift() . "\e[0m" . "\n"); }
sub print_purple_bold { print("\e[1;35m" . shift() . "\e[0m" . "\n"); }
sub must_be_root      { if($EFFECTIVE_USER_ID != 0) { print_cyan_bold('Error: You must be root to do this'); exit(1); } }
sub fatal             { print_cyan_bold('Error: ' . $_[0] . "\n"); clean(); exit(1); }
